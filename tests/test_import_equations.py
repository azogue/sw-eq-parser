# -*- coding: cp1250 -*-
"""
Test the equation parser to analyze SW parametric variables.
"""
import json
from pathlib import Path

from attr_deserialize import deserialize_struct, serialize_struct

from sw_eq_parser.sw_eq_collection import SWEquationCollection

ENCODING = "cp1250"

# def _show_unsolved_equations(eqs: List[SWEquation], known_vars: dict):
#     system_unknown = set()
#     print(f"BAD solution: {len(eqs)}:")
#     for eq in eqs:
#         print(
#             f"\t{eq.variable.original_name} = {eq.value.expr_name} "
#             f"== {eq.eval_value}"
#         )
#         needed_vars = eq.value.involved_variables_names
#         print(eq.value.involved_variables_names)
#         eq.value.render()
#         unknown_vars = set(needed_vars) - known_vars.keys()
#         print("\t", unknown_vars)
#         system_unknown.update(unknown_vars)
#
#     print(f"TOTAL: {len(system_unknown)} UNKNOWN VARS: {system_unknown}")


def test_import_equations_from_copy_operation():
    path_examples = Path(__file__).parent / "example_copied_equations.txt"
    with open(path_examples, encoding=ENCODING) as f:
        example_raw = "\n".join(f.readlines()[1:])

    known_vars = {}
    eq_system = SWEquationCollection.parse_from_copied_rows(example_raw)
    eq_system.solve_equations(known_vars, update_eval=True)
    eq_system.show_results()
    # if bad_eqs:
    #     _show_unsolved_equations(bad_eqs, known_vars)
    # print("*" * 100)
    # print(known_vars)

    # Test (de)serialization
    new_eq_system = deserialize_struct(
        SWEquationCollection,
        json.loads(json.dumps(serialize_struct(eq_system)), encoding=ENCODING),
    )
    # new_eq_system.show_results()
    assert new_eq_system == eq_system


def test_import_equations_from_collected_data():
    path_example_collected = Path(__file__).parent / "example_collected.json"
    with open(path_example_collected, encoding=ENCODING) as f:
        example_raw = json.load(f)

    known_vars = {}
    eq_system = SWEquationCollection.parse_from_collected_data(example_raw)
    eq_system.solve_equations(known_vars, update_eval=True)
    eq_system.show_results()
    # if bad_eqs:
    #     _show_unsolved_equations(bad_eqs, known_vars)
    # print("*" * 100)
    # print(known_vars)

    # Test (de)serialization
    new_eq_system = deserialize_struct(
        SWEquationCollection,
        json.loads(json.dumps(serialize_struct(eq_system)), encoding=ENCODING),
    )
    # new_eq_system.show_results()
    assert new_eq_system == eq_system


def test_equation_collection_helpers():
    path_example_collected = Path(__file__).parent / "example_collected.json"
    with open(path_example_collected, encoding=ENCODING) as f:
        example_raw = json.load(f)

    eq_system = SWEquationCollection.parse_from_collected_data(example_raw)
    eq_i = eq_system.equations[3]
    eq_subset = SWEquationCollection(equations=eq_system.equations[3:10])
    assert len(eq_system) == 151
    assert "LxL@VSTUP plech" in eq_system
    assert '"lxl@vstup plech"' in eq_system
    assert "LxL@VSTUP_plech" not in eq_system
    assert eq_i in eq_system
    assert eq_i.variable in eq_system
    assert eq_subset in eq_system
    assert next(filter(lambda x: x == eq_i, eq_system)) == eq_i
    assert path_example_collected not in eq_system
    assert eq_system[3] == eq_i
