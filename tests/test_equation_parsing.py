# -*- coding: cp1250 -*-
"""
Test the equation parser to analyze SW parametric variables.
"""
import json
from math import pi, exp
from pathlib import Path

import pytest

from attr_deserialize import deserialize_struct, serialize_struct

from sw_eq_parser import SWEquation, SWExpressionTree, SWVariable
from sw_eq_parser.sw_equation import (
    SWExprType,
    USE_NULL_VALUE_FOR_UNASSIGNED_VARS,
)

ENCODING = "cp1250"


def test_cant_parse():
    if USE_NULL_VALUE_FOR_UNASSIGNED_VARS:
        null_1 = SWExpressionTree.parse("")
        null_2 = SWExpressionTree.parse(" ")
        assert null_1.ex_type == SWExprType.SPECIAL_VALUE
        assert null_1.expr_name == "NULL"
        assert null_1.variable == null_2.variable
        assert null_1 == null_2
    else:
        with pytest.raises(ValueError):
            SWExpressionTree.parse(" ")

        with pytest.raises(ValueError):
            SWExpressionTree.parse("")

    # bad numbers
    with pytest.raises(ValueError):
        SWExpressionTree.parse("12.3.4mm")
    with pytest.raises(ValueError):
        SWExpressionTree.parse("12a.3")

    # bad unit
    with pytest.raises(ValueError):
        SWExpressionTree.parse("12.3mma")


def test_basic_expressions():
    example_complex_var_name = '"D5@DIME@N015U-01-00&DV&DV 70<1>.Assembly"'
    example_complex_var_name2 = '"D1@Pole t�sn�n� a �rouby mezi st�ny, fan3"'

    ex_var = SWVariable.parse(example_complex_var_name)
    assert ex_var.original_name == example_complex_var_name.strip('"')
    assert ex_var.expr_name == "D5@DIME@N015U__01__00&DV&DV;70[1].Assembly"

    ex_var2 = SWVariable.parse(example_complex_var_name2)
    assert ex_var2.original_name == example_complex_var_name2.strip('"')
    assert ex_var2.expr_name == "D1@Pole;t�sn�n�;a;�rouby;mezi;st�ny;;;fan3"

    ex_eq = (
        f'iif ( "fan_H" != "fan_LxL" , '
        f'( "am_vyska_vni" / {example_complex_var_name} ) / 2 - 0.001 , '
        f'"fan_H1" + "Z_lista_vyska@ER_SLB" + "fan_slbl_vyska" )'
    )
    expresion = SWExpressionTree.parse(ex_eq)
    assert (
        expresion.expr_name == "cond_fan_H__neq__fan_LxL"
        "__t_div_div_am_vyska_vni__D5@DIME@N015U"
        "__01__00&DV&DV;70[1].Assembly__i2__m__f0"
        "__f_p_fan_H1__s__p_Z_lista_vyska@ER_SLB"
        "__s__p_fan_slbl_vyska"
    )
    assert expresion.involved_variables_names == [
        "am_vyska_vni",
        example_complex_var_name.strip('"'),
        "fan_H",
        "fan_H1",
        "fan_LxL",
        "fan_slbl_vyska",
        "Z_lista_vyska@ER_SLB",
    ]

    # fmt: off
    ex_vars = dict(zip(
        ['am_vyska_vni', 'fan_H', 'fan_H1', 'fan_LxL',
         example_complex_var_name.strip('"'), 'fan_slbl_vyska',
         'Z_lista_vyska@ER_SLB'],
        [-1., 4.2, 4.4, 1., 2., 3., 4.]
    ))
    # fmt: on
    # print(expresion.solve(ex_vars))
    assert expresion.solve(ex_vars, strict=True) == -0.251


def _check_expression(eq: str, known_values: dict, expected_result: float):
    expr = SWExpressionTree.parse(eq)
    # expr.render()
    # print(f"* {eq} -> {expr.expr_name}")
    value = expr.solve(known_values, strict=True)
    # print(f"\t = {value} -> {known_values}")
    assert value == expected_result


def test_parse_equations():
    values = dict(zip(["va", "vb", "vc", "vd"], [1, 2, 3, 4]))
    eqs = [
        ('( "va" - "vb" - ( ( "vc" - 1 ) * "vd" ) ) / "vc"', -3.0),
        ('"va" - "vb" - "vc" + 1 - "vd" / "vb"', -5.0),
        ('"va" - "vb" - "vc"', -4.0),
        ('-"va" - "vb" - "vc"', -6.0),
        ('"va" * "vb" * "vc"', 6.0),
        ('"vb" * "vc" / "vd"', 1.5),
        ('"vb" * "vc" - "vd"', 2.0),
        ('"vb" - "vc" * "vd"', -10.0),
        ('"vb" * ( "vc" - "vd" )', -2.0),
        ('( "vb" - "vc" ) * "vd"', -4.0),
        ('("vb" - "vc") * "vd"', -4.0),
        ('("vb" + "vc")', 5.0),
        ('INT("vb" + "vc")', 5),
        ('INT("vb" + "vc") -2mm', 3),
        ('abs ( -sqr ( "va" * "vd" )  - log (exp( "vb" ) + 2 - 2) )', 4),
        ('abs ( -sqr ( "va" * "vd" )  - log (exp( "vb" ) + 0) )', 4),
        ('abs ( -sqr ( "va" * "vd" )  - log (exp( "vb" ) ) )', 4),
        ('sgn ( -sqr ( "va" * "vd" )  - log (exp( "vb" ) ) )', -1),
        ('"vb" ^( "vd" - 1)  - 1', 7),
        ('"vb" * ( pi - "vd" + 4)', 2 * pi),
        ('49 / ("vb" ^( "vd" - 1)  - 1)', 7),
        ('sgn(exp ( "vd" ) ) ', 1),
        ('exp(sgn ( "vd" ) ) ', exp(1)),
    ]

    [
        _check_expression(eq, known_values=values, expected_result=res)
        for eq, res in eqs
    ]


def test_solve_conditionals():
    expr = SWExpressionTree.parse(
        'iif ( "fan_Dsa" < 520 , 45 , iif ( "fan_Dsa" < 1000 , 65 , 90 ) )'
    )
    assert expr.solve({"fan_Dsa": 900}) == 65.0
    assert expr.solve({"fan_Dsa": 1900}) == 90.0
    assert expr.solve({"fan_Dsa": 100}) == 45.0

    values = dict(zip(["va", "vb", "vc", "vd"], [1, 2, 3, 4]))
    # fmt: off
    # noinspection PyPep8
    eqs = [
        ('iif(INT("vb" + "vc") < 4, INT("vb" + "vc") -2mm, '
         'INT("vb" + "vc") +3. )', 8.),
        ('iif(INT("vb" + "vc") < 6mm, INT("vb" + "vc") -2mm, '
         'INT("vb" + "vc") +3. )', 3),
        ('iif(INT("vb" + "vc") = 5, INT("vb" + "vc") -2mm, '
         'INT("vb" + "vc") +3. )', 3),
        ('iif(INT("vb" + "vc") > 2kg, INT("vb" + "vc") -2mm, '
         'INT("vb" + "vc") +3. )', 3),
        ('iif (  ( "vb" = 2 )  AND  ("vc" = 3 )  AND  ( "vd" > 1 )  ,  '
         'unsuppressed  ,  suppressed  ) ', '"unsuppressed"'),
        ('iif (  ( "vb" = 2 )  AND  ("vc" < 3 )  AND  ( "vd" > 1 )  ,  '
         'unsuppressed  ,  suppressed  ) ', '"suppressed"'),
        ('iif ( "va" > 499 , "suppressed" , iif ( "vd" < > 4 , '
         '"suppressed" , "unsuppressed" ) )', '"unsuppressed"'),
        ('iif ( "va" > 499 , "suppressed" , iif ( "vb" < > 4 , '
         '"suppressed" , "unsuppressed" ) )', '"suppressed"'),
        ('iif ( "vd" =  > 2.0 * "vb" ,  suppressed  ,  '
         'unsuppressed  ) ', '"suppressed"'),
    ]
    # fmt: on
    [
        _check_expression(eq, known_values=values, expected_result=res)
        for eq, res in eqs
    ]


def test_deal_with_unknown_vars():
    values = dict(zip(["va", "vb", "vc", "vd"], [1, 2, 3, 4]))
    assert SWExpressionTree.parse('- "vb" + 3.1').solve(values) == 1.1
    assert SWExpressionTree.parse(' - "unknown" + 3.1').solve(values) == 3.1
    with pytest.raises(ValueError):
        SWExpressionTree.parse('-"unknown" + 3.1').solve(values, strict=True)


def test_special_values():
    expr = SWExpressionTree.parse(
        ' iif ( "fan_Z_lista_vne" = 0 , "suppressed" , "unsuppressed" )'
    )
    assert expr.solve({"fan_Z_lista_vne": 1}) == '"unsuppressed"'
    assert expr.solve({"fan_Z_lista_vne": 0}) == '"suppressed"'
    variables = list(
        filter(
            lambda x: x.has_value
            and x.variable_type == SWExprType.SPECIAL_VALUE,
            expr.get_variables(),
        )
    )
    # print(expr.get_variables())
    assert len(variables) == 2
    values = [v.eval_value({}, strict=True) for v in variables]
    assert isinstance(values[0], str)
    assert isinstance(values[1], str)
    assert values == ['"suppressed"', '"unsuppressed"']


def test_parse_equation_collection():
    path_examples = Path(__file__).parent / "example_equations.txt"
    with open(path_examples, encoding=ENCODING) as f:
        example_raw = f.read()

    eqs = []
    for l in example_raw.splitlines()[1:]:
        v_name, expression = l.split("=", maxsplit=1)
        try:
            expr = SWExpressionTree.parse(v_name, expression)
            if expr is not None:
                eqs.append(
                    SWEquation(
                        raw_value=l,
                        variable=SWVariable.parse(v_name),
                        value=expr,
                    )
                )
                # eq.render()
            else:
                print(f"BAD EQ: '{l}'")
        except ValueError as exc:
            print(l, exc)
            raise exc

    # Test serialization/deserialization
    def _de_serial(objectclass, obj):
        data_str = json.dumps(serialize_struct(obj))
        # print(data_str)

        new_obj = deserialize_struct(
            objectclass, json.loads(data_str, encoding=ENCODING), strict=True
        )
        return data_str, new_obj

    for eq in eqs:
        data, new_eq = _de_serial(SWEquation, eq)
        assert new_eq == eq


def test_suppressed_vars():
    raw_eq = (
        '"N014-01A-00U&VENTIL�TOR&D�L�C� ST�NA TYP A-1.Assembly"= '
        'iif ( "fan_deska_vcelku@Layout_FAN_OK<1>.Part" = 0 ,  suppressed  , '
        'iif ( "fan_deska_vysuvna" = 1 ,  suppressed  , '
        'iif ( "fan_deska_vcelku_typ_B@Layout_FAN_OK<1>.Part" = 0 ,  '
        "unsuppressed  ,  suppressed  )  )  ) "
    )

    eq = SWEquation.parse_from_collected((raw_eq, 1.0))
    # print(eq.value.involved_variables_names)
    assert eq.value.involved_variables_names == [
        "fan_deska_vcelku@Layout_FAN_OK<1>.Part",
        "fan_deska_vcelku_typ_B@Layout_FAN_OK<1>.Part",
        "fan_deska_vysuvna",
    ]
    eq.render()
    # print(eq.solve({
    #     'fan_deska_vcelku@Layout_FAN_OK<1>.Part': 1,
    #     'fan_deska_vcelku_typ_B@Layout_FAN_OK<1>.Part': 0,
    #     'fan_deska_vysuvna': 0
    # }))
    assert (
        eq.solve(
            {
                "fan_deska_vcelku@Layout_FAN_OK<1>.Part": 1,
                "fan_deska_vcelku_typ_B@Layout_FAN_OK<1>.Part": 0,
                "fan_deska_vysuvna": 0,
            }
        )
        == '"unsuppressed"'
    )
    assert (
        eq.solve(
            {
                "fan_deska_vcelku@Layout_FAN_OK<1>.Part": 0,
                "fan_deska_vcelku_typ_B@Layout_FAN_OK<1>.Part": 0,
                "fan_deska_vysuvna": 0,
            }
        )
        == '"suppressed"'
    )
    assert (
        eq.solve(
            {
                "fan_deska_vcelku@Layout_FAN_OK<1>.Part": 1,
                "fan_deska_vcelku_typ_B@Layout_FAN_OK<1>.Part": 0,
                "fan_deska_vysuvna": 1,
            }
        )
        == '"suppressed"'
    )
    assert (
        eq.solve(
            {
                "fan_deska_vcelku@Layout_FAN_OK<1>.Part": 1,
                "fan_deska_vcelku_typ_B@Layout_FAN_OK<1>.Part": 1,
                "fan_deska_vysuvna": 0,
            }
        )
        == '"suppressed"'
    )
